package sample

import cnames.structs.GLFWwindow
import glfw3.*
import kotlinx.cinterop.CPointer
import platform.OpenGL3.GL_COLOR_BUFFER_BIT
import platform.OpenGL3.glClear

const val FALSE = 0

typealias GameWindow = CPointer<GLFWwindow>

fun main() {
    if(glfwInit() == FALSE) {
        error("Failed to init")
    }

    println("GLFW v$GLFW_VERSION_MAJOR.$GLFW_VERSION_MINOR.$GLFW_VERSION_REVISION has been loaded.")

    val window = glfwCreateWindow(640, 480, "Hello World",
        null, null)
    if (window == null) {
        glfwTerminate()
        error("Failed to create game window!")
    }
    window.makeContextCurrent()
    while (!window.shouldClose()) {
        glClear(GL_COLOR_BUFFER_BIT)
        window.swapBuffers()
        glfwSwapInterval(1)
        glfwPollEvents()
    }
    glfwTerminate()
}

fun GameWindow.makeContextCurrent() = glfwMakeContextCurrent(this)
fun GameWindow.shouldClose() = glfwWindowShouldClose(this) != 0
fun GameWindow.swapBuffers() = glfwSwapBuffers(this)